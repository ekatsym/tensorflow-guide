FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04

MAINTAINER ekatsym

ENV DEBIAN_FRONTEND noninteractive

RUN apt update

# Install python and pipenv
RUN apt install -y git-core python3 python3-pip python3-venv python3-tk
RUN ln -s /usr/bin/python3 /usr/bin/python \
 && ln -s /usr/bin/pip3 /usr/bin/pip
RUN pip install --upgrade pip
RUN pip install pipenv
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

# Enable GUI

# Get user arguments
ARG user
ARG uid
ARG gid

# Add new user
ENV USERNAME $user
RUN useradd -m $USERNAME \
 && echo "$USERNAME:$USERNAME" | chpasswd \
 && usermod --shell /bin/bash $USERNAME \
 && usermod --uid $uid $USERNAME \
 && groupmod --gid $gid $USERNAME

# Install TensorFlow docs
VOLUME /home/$user/tensorflow
RUN git clone https://github.com/tensorflow/docs /home/$user/tensorflow/tensorflow_docs

# User
USER $USERNAME
WORKDIR /home/$user/tensorflow

CMD ["bash"]

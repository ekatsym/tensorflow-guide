#!/bin/bash
docker run -itd --name tensorflow-guide\
    --net host\
    -v $HOME/.Xauthority:$HOME/.Xauthority:rw\
    -v $PWD:$HOME/tensorflow\
    -e DISPLAY=$DISPLAY\
    -e TERM=$TERM\
    --gpus all\
    ekatsym/tensorflow-guide

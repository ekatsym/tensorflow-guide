import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


model1 = keras.Sequential(
    [
        layers.Dense(2, activation='relu', name='layer1'),
        layers.Dense(3, activation='relu', name='layer2'),
        layers.Dense(4, name='layer3')
    ]
)
x = tf.ones((3, 3))
y = model1(x)


layer1 = layers.Dense(2, activation='relu', name='layer1')
layer2 = layers.Dense(3, activation='relu', name='layer2')
layer3 = layers.Dense(4, name='layer3')
x = tf.ones((3, 3))
y = layer3(layer2(layer1(x)))


model2 = keras.Sequential(
    [
        layers.Dense(2, activation='relu'),
        layers.Dense(3, activation='relu'),
        layers.Dense(4),
    ]
)

model3 = keras.Sequential()
model3.add(layers.Dense(2, activation='relu'))
model3.add(layers.Dense(3, activation='relu'))
model3.add(layers.Dense(4))

model4 = keras.Sequential()
model4.add(layers.Dense(2, activation='relu', name='layer1'))
model4.add(layers.Dense(3, activation='relu', name='layer2'))
model4.add(layers.Dense(4, name='layer3'))

model5 = keras.Sequential(
    [
        layers.Dense(2, activation='relu'),
        layers.Dense(3, activation='relu'),
        layers.Dense(4),
    ]
)
x = tf.ones((1, 4))
y = model5(x)
print("Number of weights after calling the model:", len(model5.weights))

model5.summary()


model6 = keras.Sequential()
model6.add(keras.Input(shape=(4,)))
model6.add(layers.Dense(2, activation='relu'))

model6.summary()


model7 = keras.Sequential()
model7.add(keras.Input(shape=(250, 250, 3)))
model7.add(layers.Conv2D(32, 5, strides=2, activation='relu'))
model7.add(layers.Conv2D(32, 3, activation='relu'))
model7.add(layers.MaxPool2D(3))
model7.summary()
model7.add(layers.GlobalMaxPool2D())
model7.add(layers.Dense(10))
